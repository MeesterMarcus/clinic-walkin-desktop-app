import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import * as moment from 'moment'; // add this 1 of 4

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayedColumns = ['position', 'name', 'arrivalTime', 'appointmentTime', 'appointmentWish'];
  dataSource : any;
  elementData: Element[];
  

  constructor(private electronService : ElectronService) { }

  ngOnInit() {
    this.initElementData();
    let now = moment();
    console.log(now);
  }

  getNowArrival(element) {
    let now = moment();
    element.arrivalTime = now;
  }

  getNowAppt(element) {
    let now = moment();
    element.appointmentTime = now;
  }

  save() {
    let response = this.electronService.ipcRenderer.sendSync('getSomething',this.elementData);
    console.log(response);
  }

  initElementData() {
    this.elementData = [];
    for (let i = 0; i < 24; i++) {
      let element = {} as Element;
      element.position = i+1;
      this.elementData.push(element);
    }
    this.dataSource = this.elementData;
  }

}

export interface Element {
  position: number;
  name: string;
  arrivalTime: string;
  appointmentTime: string;
  appointmentWish: string;
}